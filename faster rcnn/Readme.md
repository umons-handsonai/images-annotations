# Detectron - Implementation

Detectron is Facebook AI Research's software system that implements state-of-the-art object detection algorithms. It is written in Python and powered by the Caffe2 deep learning framework.

Detectron only works with GPU, no CPU implementation yet (on master branch, check other branches but might not be stable)

## Git directories : 
- [Detectron](https://github.com/facebookresearch/Detectron)
- [Keys database](https://gitlab.com/umons-handsonai/images-annotations)

## Requierements :
- Nvidia : [Cuda](https://developer.nvidia.com/cuda-downloads) / [Cudnn](https://developer.nvidia.com/cudnn) / [Nccl](https://developer.nvidia.com/nccl)
- Python : Anaconda / Python3 or 2.7

## Installation :
### Caffe2 :
for details check : [this](https://caffe2.ai/docs/getting-started.html?platform=ubuntu&configuration=prebuilt)

#### On Ubuntu (with conda):

Anaconda setup : 

    conda install -U pip setuptools
    conda install \
            graphviz \
            hypothesis \
            ipython \
            jupyter \
            matplotlib \
            notebook \
            pydot \
            python-nvd3 \
            pyyaml \
            requests \
            scikit-image \
            scipy

Caffe setup : 

    We build Linux packages without CUDA support, with CUDA 9.0 support, and with CUDA 8.0 support, for both Python 2.7 and Python 3.6. These packages are built on Ubuntu 16.04, but they will probably work on Ubuntu14.04 as well (if they do not, please tell us by creating an issue on our Github page). To install Caffe2 with Anaconda, simply activate your desired conda environment and then run one of the following commands

**For GPU support you will need CUDA, CuDNN, and NCCL. These must be installed from Nvidia’s website.**

For Caffe2 with CUDA 9 and CuDNN 7 support (also tested on CUDA 10, CuDNN 7.3.1.20) : 

    conda install pytorch-nightly -c pytorch

For Caffe2 with CUDA 8 and CuDNN 7 support :

    conda install pytorch-nightly cuda80 -c pytorch

To test the installation, run these commands : 

    # To check if Caffe2 build was successful
    python -c 'from caffe2.python import core' 2>/dev/null && echo "Success" || echo "Failure"

    # To check if Caffe2 GPU build was successful
    # This must print a number > 0 in order to use Detectron
    python -c 'from caffe2.python import workspace; print(workspace.NumCudaDevices())'

If the caffe2 Python package is not found, you likely need to adjust your PYTHONPATH environment variable to include its location (/path/to/caffe2/build, where build is the Caffe2 CMake build directory).

**Other Dependencies :**

Install the COCO API:

    # COCOAPI=/path/to/clone/cocoapi
    git clone https://github.com/cocodataset/cocoapi.git $COCOAPI
    cd $COCOAPI/PythonAPI
    # Install into global site-packages make install
    # Alternatively, if you do not have permissions or prefer
    # not to install the COCO API into global site-packages
    python setup.py install --user

Note that instructions like # COCOAPI=/path/to/install/cocoapi indicate that you should pick a path where you'd like to have the software cloned and then set an environment variable (COCOAPI in this case) accordingly.

#### Basic Detectron Install :

Clone the Detectron repository:

    # DETECTRON=/path/to/clone/detectron
    git clone https://github.com/facebookresearch/detectron $DETECTRON

Install Python dependencies:

    pip install -r $DETECTRON/requirements.txt

Set up Python modules:

    cd $DETECTRON && make

Check that Detectron tests pass (e.g. for SpatialNarrowAsOp test):

    python $DETECTRON/detectron/tests/test_spatial_narrow_as_op.py

#### Implementation of keys detection : 

**Data Preparation :**

For details check : [this](https://github.com/facebookresearch/Detectron/blob/master/detectron/datasets/data/README.md)

For PASCAL VOC :

We assume that your symlinked detectron/datasets/data/VOC2018 directory has the following structure:

    VOC2018
    |_ JPEGImages
    |  |_ <im-1-name>.jpg
    |  |_ ...
    |  |_ <im-N-name>.jpg
    |_ annotations
    |  |_ voc_2018_train.json
    |  |_ voc_2018_val.json
    |  |_ ...
    |_ VOCdevkit2018

Create symlinks for VOC2018:

    mkdir -p $DETECTRON/detectron/datasets/data/VOC2018
    ln -s /path/to/VOC2018/JPEGImages $DETECTRON/detectron/datasets/data/VOC2018/JPEGImages
    ln -s /path/to/VOC2018/json/annotations $DETECTRON/detectron/datasets/data/VOC2018/annotations
    ln -s /path/to/VOC2018/devkit $DETECTRON/detectron/datasets/data/VOC2018/VOCdevkit2018

the "voc_2018_train.json" is a PASCAL VOC annotations converted to COCO json format (check this script to change from VOC to coco format : [script](https://github.com/CivilNet/Gemfield/blob/master/src/python/pascal_voc_xml2json/pascal_voc_xml2json.py))

(to generate the xml files use the tool : [boobs](https://medium.com/@drainingsun/boobs-yolo-bbox-annotation-tool-96fb765d0036) while importing all the images from the shared git images-annotations)


To use it, prepare all your Training (this will be repeated for validation files) voc xml files in a folder called :

    .
    |-- Annotations
    |       |-- 1.xml
    |       |-- 2.xml
    |       |-- 3.xml
    |       |__ ...
    |-- pascal_voc_xml2json.py
    |__...
    
once that is done execute the script :

    python pypascal_voc_xml2json.py

The generated file will be called instance.json, rename it : 

    mv instance.json voc_2018_train.json

Repeat this manipulation for the validation voc xml files :

    python pypascal_voc_xml2json.py

    mv instance.json voc_2018_val.json

once those 2 json files created, move them to : 

    $DETECTRON/detectron/datasets/data/VOC2018/annotations

Move all the images (train and val) to the folder :

    $DETECTRON/detectron/datasets/data/VOC2018/JPEGImages

Leave the Devkit folder empty.

Now that the data is filled, we need to set it up for detectron to see : 
Go to : 

    $DETECTRON/detectron/datasets/dataset_catalog.py

Open and edit it : 
    Change the variable "_DATASETS" (line 39) to : 


    # Available datasets
    _DATASETS = {
        'voc_2018_train': {
            _IM_DIR:
                _DATA_DIR + '/VOC2018/JPEGImages',
            _ANN_FN:
                _DATA_DIR + '/VOC2018/annotations/voc_2018_train.json',
            _DEVKIT_DIR:
                _DATA_DIR + '/VOC2018/VOCdevkit2018'
        },
        'voc_2018_val': {
            _IM_DIR:
                _DATA_DIR + '/VOC2018/JPEGImages',
            _ANN_FN:
                _DATA_DIR + '/VOC2018/annotations/voc_2018_val.json',
            _DEVKIT_DIR:
                _DATA_DIR + '/VOC2018/VOCdevkit2018'
        }
    }

Go to : 

    $DETECTRON/detectron/datasets/dummy_datasets.py

Open and edit it : 
    Change the variable classes (line 31) to : 

    classes = [
        '__background__', 'keys'
    ]

**Backup weights snapshots :**

To change the rate of saving of checkpoint weights you will need to change this file : 

    $DETECTRON/detectron/core/config.py

At line 123 : 

    __C.TRAIN.SNAPSHOT_ITERS = 1000

This means that at every 1000 iteration a ckpt (checkpoint/backup weight) file will be saved : 

    iteration 999
    iteration 1999
    iteration 2999
    ...

**Config file :**

The config file contains the general information for the training. Depending on the number of GPUs you are using : 

    # This must print a number > 0 in order to use Detectron
    python -c 'from caffe2.python import workspace; print(workspace.NumCudaDevices())'

use the corresponding yaml file : 
For 1 GPU : 

    $Detectron/configs/getting_started/tutorial_1gpu_e2e_faster_rcnn_R-50-FPN.yaml

Edit it : 

line 4 : 

    NUM_CLASSES = 2

As there are the keys and background class

line 12 

    MAX_ITER: 20000

Maximum iteration before training stop.

line 42 

      DATASETS: ('voc_2018_train',)

change dataset name to the name added to the catalog in the Data preparation chapter.

line 49

      DATASETS: ('voc_2018_val',)

Same for the validation data.

### Detectron train : 

go to the $DETECTRON folder and execute : 

    # cfg : config file
    # OUTPUT_DIR : directory where checkpoint weights are saved

    python tools/train_net.py \
    --cfg configs/getting_started/tutorial_1gpu_e2e_faster_rcnn_R-50-FPN.yaml \
    OUTPUT_DIR /tmp/detectron-output

When stopped the training starts at the latest checkpoint automatically, if saved.
### Detectron Test : 

go to the $DETECTRON folder and execute : 

    # wts : weights
    # image-ext : image extension (jpg,png,...)
    # testing:  test image folder (can be replaced by one image : testing/test2.jpg and delete the image-ext line)

    python tools/infer_simple.py --cfg configs/getting_started/defi2_1gpu_e2e_faster_rcnn_R-50-FPN.yaml \
        --wts checkpoints/model_iter6999.pkl \
        --image-ext jpg \
        --output-dir results \
        testing