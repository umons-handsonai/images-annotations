# Contributing to Images Annotations

Thanks to you for enhancing the images and annotation data base.

## Annotation

Simply pick-up whichever picture you want from the to-annotate folder and once they are annotated move them with the associated yolo annotaiton text file in the annotated folder.

### Adding self made picture

Adding self made picture is a good idea, because google's images found by AI are probably already to good candidates, so please feel free to take pictures of keys, annotate them and upload the picture + annotation in here, in the annotated folder directly.

## Submission guideline

Please always push a branch and make a pull request (PR), wait to someone to merge your PR or ask on the slack channel someone to review and merge.
