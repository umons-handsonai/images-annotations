# images-annotations

Common effort to generate data and annotate images

## Way of working

Contribute by taking picture (your own or from the internet) annotating them and adding them to this common shared annotated data base.

The annotation notation is the [YOLO](https://github.com/AlexeyAB/darknet#how-to-train-to-detect-your-custom-objects) notation.

### Yolo notation

#### Images and annotation files

* Image filename: seba-1234.jpg
* Annotation filename: seba-1234.txt

Annotation file 1234.txt will contains

0 0.687109 0.379167 0.255469 0.158333

Where **0** is the line in the classes.txt file corresponding to the class of the object (see [classes.txt](classes.txt)), here first line is line 0 and correspond to the class: **"key"**

Where **0.687109 0.379167 0.255469 0.158333** are in the order the x, y coordinate of the center of the box and the width, height value of the box. All the value are percentage of the height and width of the images. In this case the center of the box is at 68.71% and 37.91% of the top left corner of the image and the box size is 25.54% of the widht of the image and 15.83% of the height of the image.

#### Train file

The train file must contains a list of all the images added to the annotated classes folder.

Ex:

    annotated-classes/sebastien_barbieri/seba-0001.jpg
    annotated-classes/sebastien_barbieri/seba-0002.jpg
    annotated-classes/sebastien_barbieri/seba-0003.jpg
    annotated-classes/nelson_lopes/nelson-0001.jpg
    annotated-classes/nelson_lopes/nelson-0002.jpg
    annotated-classes/nelson_lopes/nelson-0003.jpg

To generate the train.txt and test.txt files, run the script from the "trainGen.py" with the following command : 
    
    python trainGen.py -i annotated-classes -t 0.75 -s 123456

The "-t" stands for the train ratio and the "-s" for the seed (these params are optional). The default values for those 2 are, respectively, 0.7 and 2018. To generate another random list of train and test you will need to change the seed.

The trainGen script needs to be on the same level as the annotated-classes folder.

When you replace the original train.txt keep in mind that you will need to have the "annotated-classes" folder on the same level as the darknet script :
    
    darknet-master
    ├── keys_detection
    |   ├── train.txt           # the train file should be here
    |   ├── test.txt            # the test file should be here
    |   └── ...                                                
    ├── darknet.py                   
    ├── Makefile.example
    ├── annotated-classes       # the folder should be here
    └──...